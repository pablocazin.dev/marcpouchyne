<?php

namespace App\Controller;

use App\Entity\Carroussel;
use App\Form\CarrousselType;
use App\Repository\CarrousselRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/carroussel')]
class CarrousselController extends AbstractController
{
    #[Route('/', name: 'app_carroussel_index', methods: ['GET'])]
    public function index(CarrousselRepository $carrousselRepository): Response
    {
        return $this->render('carroussel/index.html.twig', [
            'carroussels' => $carrousselRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_carroussel_new', methods: ['GET', 'POST'])]
    public function new(Request $request, FileUploader $fileUploader, CarrousselRepository $carrousselRepository): Response
    {
        $carroussel = new Carroussel();
        $form = $this->createForm(CarrousselType::class, $carroussel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $carroussel->setImage($imageFileName);
            }
            $carrousselRepository->add($carroussel);
            return $this->redirectToRoute('app_carroussel_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('Admin/Carroussel/new.html.twig', [
            'carroussel' => $carroussel,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_carroussel_show', methods: ['GET'])]
    public function show(Carroussel $carroussel): Response
    {
        return $this->render('carroussel/show.html.twig', [
            'carroussel' => $carroussel,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_carroussel_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Carroussel $carroussel, CarrousselRepository $carrousselRepository): Response
    {
        $form = $this->createForm(CarrousselType::class, $carroussel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $carrousselRepository->add($carroussel);
            return $this->redirectToRoute('app_carroussel_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('carroussel/edit.html.twig', [
            'carroussel' => $carroussel,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_carroussel_delete', methods: ['POST'])]
    public function delete(Request $request, Carroussel $carroussel, CarrousselRepository $carrousselRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$carroussel->getId(), $request->request->get('_token'))) {
            $carrousselRepository->remove($carroussel);
        }

        return $this->redirectToRoute('app_carroussel_index', [], Response::HTTP_SEE_OTHER);
    }
}
