<?php

namespace App\Controller;

use App\Entity\Carroussel;
use App\Form\CarrousselType;
use App\Repository\CarrousselRepository;
use App\Repository\InfosRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index', methods: ['GET'])]
    public function index(CarrousselRepository $carrousselRepository, InfosRepository $infosRepository): Response
    {
        return $this->render('index/index.html.twig', [
            'carroussels' => $carrousselRepository->findAll(),
            'infos' => $infosRepository->findAll(),
        ]);
    }
}
