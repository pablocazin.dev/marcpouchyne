<?php

namespace App\Controller;

use App\Entity\Carroussel;
use App\Form\CarrousselType;
use App\Repository\CarrousselRepository;
use App\Entity\Infos;
use App\Form\InfosType;
use App\Repository\InfosRepository;
use App\Entity\Commentaires;
use App\Repository\CommentairesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Polyfill\Intl\Idn\Info;
use App\Repository\AnimationsRepository;

class AdminController extends AbstractController
{

    #[Route('/admin', name: 'app_admin', methods: ['GET', 'POST'])]
    public function index(AnimationsRepository $animationsRepository, Request $request,CommentairesRepository $commentairesRepository, InfosRepository $infosRepository): Response
    {
        $info = $infosRepository->find(1);
        if(!$info){
            $info = new Info();
        }
        $form = $this->createForm(InfosType::class, $info);
        $form->handleRequest($request);

        
        if ($form->isSubmitted() && $form->isValid()) {
            $infosRepository->add($info);
            return $this->redirectToRoute('app_admin', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/index.html.twig', [
            'commentaires' => $commentairesRepository->findAll(),
            'animations' => $animationsRepository->findAll(),
            'form' => $form,
            
        ]);
    }

     // #[Route('/{id}/edit', name: 'app_infos_edit', methods: ['GET', 'POST'])]
    // public function edit(Request $request, Infos $info, InfosRepository $infosRepository): Response
    // {
    //     $form = $this->createForm(InfosType::class, $info);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $infosRepository->add($info);
    //         return $this->redirectToRoute('app_infos_index', [], Response::HTTP_SEE_OTHER);
    //     }

    //     return $this->renderForm('infos/edit.html.twig', [
    //         'info' => $info,
    //         'form' => $form,
    //     ]);
    // }



    #\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\_ Commentaires \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    #[Route('/commentaire/{id}/valide', name: 'admin_commentaires_edit', methods: ['GET', 'POST'])]
    public function valid(CommentairesRepository $commentairesRepository, int $id): Response
    {
        $com = $commentairesRepository->find($id);
        $com->setEtat(1);
        $commentairesRepository->add($com);
        return $this->redirectToRoute('app_admin', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}', name: 'admin_commentaires_delete', methods: ['POST'])]
    public function delete(Request $request, Commentaires $commentaire, CommentairesRepository $commentairesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$commentaire->getId(), $request->request->get('_token'))) {
            $commentairesRepository->remove($commentaire);
        }

        return $this->redirectToRoute('app_admin', [], Response::HTTP_SEE_OTHER);
    }
}
