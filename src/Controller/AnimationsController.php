<?php

namespace App\Controller;

use App\Entity\Animations;
use App\Form\AnimationsType;
use App\Repository\AnimationsRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/animations')]
class AnimationsController extends AbstractController
{
    #[Route('/', name: 'app_animations_index', methods: ['GET'])]
    public function index(AnimationsRepository $animationsRepository): Response
    {
        return $this->render('animations/index.html.twig', [
            'animations' => $animationsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_animations_new', methods: ['GET', 'POST'])]
    public function new(Request $request, FileUploader $fileUploader, AnimationsRepository $animationsRepository): Response
    {
        $animation = new Animations();
        $form = $this->createForm(AnimationsType::class, $animation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imageFile = $form->get('image')->getData();
            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $animation->setImage($imageFileName);
            }
            $animationsRepository->add($animation);
            return $this->redirectToRoute('app_animations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('Admin/Animations/new.html.twig', [
            'animation' => $animation,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_animations_show', methods: ['GET'])]
    public function show(Animations $animation): Response
    {
        return $this->render('animations/show.html.twig', [
            'animation' => $animation,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_animations_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Animations $animation, AnimationsRepository $animationsRepository): Response
    {
        $form = $this->createForm(AnimationsType::class, $animation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $animationsRepository->add($animation);
            return $this->redirectToRoute('app_animations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('AdminSTOCK/animations/edit.html.twig', [
            'animation' => $animation,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_animations_delete', methods: ['POST'])]
    public function delete(Request $request, Animations $animation, AnimationsRepository $animationsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$animation->getId(), $request->request->get('_token'))) {
            $animationsRepository->remove($animation);
        }

        return $this->redirectToRoute('app_animations_index', [], Response::HTTP_SEE_OTHER);
    }
}
